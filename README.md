# Selenium grid deployment script

## Cloud provider

This script use the services of Hetzner.

It should be easily modified to use other cloud providers.

## Dependencies

You need to install `jq`, `nmap` and `hcloud`, the Hetzner cloud API CLI.

On Debian
```bash
apt install jq nmap hcloud-cli
```

## Usage

Just read the help provided by the script

```bash
./create-selenium-stack.sh -h
```

To remove all servers in the context:
```bash
./create-selenium-stack.sh -d
```
