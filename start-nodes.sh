#!/bin/bash

usage() {
    cat << EOF
$(basename "$0") (c) Framasoft 2023, WTPF

USAGE
    $(basename "$0") [-h] [-n <int>]

OPTIONS
    -h               print this help and exit
    -n <int>         how many selenium nodes you want to launch. Default: 1
    -i <ip address>  the IP address of the selenium hub.
                     MANDATORY!
                     No default:
EOF
    exit "$1"
}

NUMBER=1

while getopts "hn:i:" option; do
    case $option in
        h)
            usage 0
            ;;
        n)
            NUMBER=$OPTARG
            ;;
        i)
            HUB_IP=$OPTARG
            ;;
        *)
            usage 1
            ;;
    esac
done

HOST=$(hostname)

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

echo "Installing packages"
apt-get -qq -y update
apt-get -qq -y dist-upgrade
apt-get -qq -y install jq \
                       tmux \
                       vim \
                       multitail \
                       htop \
                       liquidprompt \
                       coreutils \
                       docker.io \
                       apparmor-utils

echo "Activating liquidprompt"
liquidprompt_activate
. /usr/share/liquidprompt/liquidprompt

echo "Modifying kernel parameters"
sysctl net.ipv6.conf.default.forwarding=1
sysctl net.ipv6.conf.all.forwarding=1

echo "Configuring Docker for IPv6"
IP_ADDR=$(ip --json a show eth0 | jq '.[] | .addr_info | .[] | select(.family | contains("inet6")) | select(.scope | contains("global")) | .local' -r)
NETWORK=$(echo "$IP_ADDR" | sed -e 's@:[^:]\+$@8000::/65@')

cat << EOF > /etc/docker/daemon.json
{
  "ipv6": true,
  "fixed-cidr-v6": "$NETWORK"
}
EOF
systemctl restart docker

for NB in $(seq 1 "$NUMBER"); do
    NODE_NAME="selenium-${HOST}-node-${NB}"
    echo "Starting Selenium node n°$NB"
    docker run --rm \
               --detach \
               --shm-size=2g \
               --env="SE_EVENT_BUS_HOST=$HUB_IP" \
               --env="SE_EVENT_BUS_PUBLISH_PORT=4442" \
               --env="SE_EVENT_BUS_SUBSCRIBE_PORT=4443" \
               --name "$NODE_NAME" \
               selenium/node-chrome:latest > /dev/null 2>&1

    echo "Adding Selenium node n°$NB to neighbour proxy"
    DOCKER_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.GlobalIPv6Address}}{{end}}' "$NODE_NAME")
    ip -6 neighbour add proxy "$DOCKER_IP" dev eth0
    docker stop "$NODE_NAME"
    sleep 1
    docker run --rm \
               --detach \
               --shm-size=2g \
               --env="SE_EVENT_BUS_HOST=$HUB_IP" \
               --env="SE_EVENT_BUS_PUBLISH_PORT=4442" \
               --env="SE_EVENT_BUS_SUBSCRIBE_PORT=4443" \
               --env="SE_NODE_HOST=$DOCKER_IP" \
               --name "$NODE_NAME" \
               selenium/node-chrome:latest > /dev/null 2>&1
done
