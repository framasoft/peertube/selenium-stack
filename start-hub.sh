#!/bin/bash

usage() {
    cat << EOF
$(basename "$0") (c) Framasoft 2023, WTPF

USAGE
    $(basename "$0") [-h] [-n <int>]

OPTIONS
    -h        print this help and exit
EOF
    exit "$1"
}

NUMBER=1

while getopts "hn:" option; do
    case $option in
        h)
            usage 0
            ;;
        *)
            usage 1
            ;;
    esac
done

HOST=$(hostname)

DEBIAN_FRONTEND=noninteractive
export DEBIAN_FRONTEND

echo "Installing packages"
apt-get -qq -y update
apt-get -qq -y dist-upgrade
apt-get -qq -y install jq \
                       tmux \
                       vim \
                       multitail \
                       htop \
                       liquidprompt \
                       coreutils \
                       docker.io \
                       apparmor-utils

echo "Activating liquidprompt"
liquidprompt_activate
. /usr/share/liquidprompt/liquidprompt

echo "Modifying kernel parameters"
sysctl net.ipv6.conf.default.forwarding=1
sysctl net.ipv6.conf.all.forwarding=1

echo "Configuring Docker for IPv6"
IP_ADDR=$(ip --json a show eth0 | jq '.[] | .addr_info | .[] | select(.family | contains("inet6")) | select(.scope | contains("global")) | .local' -r)
NETWORK=$(echo "$IP_ADDR" | sed -e 's@:[^:]\+$@8000::/65@')

cat << EOF > /etc/docker/daemon.json
{
  "ipv6": true,
  "fixed-cidr-v6": "$NETWORK"
}
EOF
systemctl restart docker

NODE_NAME="selenium-hub"
echo "Starting Selenium hub"
docker run --rm \
           --detach \
           --publish "4442-4444:4442-4444" \
           --name "$NODE_NAME" \
           selenium/hub:latest > /dev/null 2>&1

echo "Adding Selenium hub to neighbour proxy"
DOCKER_IP=$(docker inspect -f '{{range.NetworkSettings.Networks}}{{.GlobalIPv6Address}}{{end}}' "$NODE_NAME")
ip -6 neighbour add proxy "$DOCKER_IP" dev eth0
docker stop "$NODE_NAME"
sleep 1
docker run --rm \
           --detach \
           --publish "4442-4444:4442-4444" \
           --name "$NODE_NAME" \
           selenium/hub:latest > /dev/null 2>&1
